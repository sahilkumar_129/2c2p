import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import axios from 'axios';

const baseUrl = 'https://sandbox-pgw.2c2p.com/payment/4.1';
@Injectable()
export class AppService {
  constructor(private jwtService: JwtService) {}
  async getPaymentTokenResponse(body: Record<string, unknown>): Promise<any> {
    console.log(body);
    const jwtToken = await this.jwtService.sign(body);
    console.log(jwtToken);
    const url = `${baseUrl}/paymentToken`;
    const requestData = { payload: jwtToken };
    const { data: responseData } = await axios.post(url, requestData);
    console.log(responseData);
    const paymentTokenResponse = await this.jwtService.decode(
      responseData.payload,
    );
    return paymentTokenResponse;
  }

  async getPaymentNotification(body: Record<string, unknown>): Promise<any> {
    console.log(body);
    return body;
  }
}

/** Ways to indentify if payment is successful
 * 1.
 * Payment notification is used to send the notification from client side to any of the social media
 * like whatsapp, facebook, line, etc.
 *
 * 2.
 * paymentInquiry api call to be made from backend
 */
