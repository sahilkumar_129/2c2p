import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { AppController } from './app.controller';
import { AppService } from './app.service';

@Module({
  imports: [
    JwtModule.register({
      secret:
        'CD229682D3297390B9F66FF4020B758F4A5E625AF4992E5D75D311D6458B38E2',
      signOptions: {
        algorithm: 'HS256',
      },
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
