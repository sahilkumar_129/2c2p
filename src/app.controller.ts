import { Body, Controller, Get, Post, ValidationPipe } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post('payment-token')
  async getPaymentTokenResponse(
    @Body(new ValidationPipe({ transform: true }))
    body: any,
  ): Promise<any> {
    return await this.appService.getPaymentTokenResponse(body);
  }

  @Post('payment-notification')
  async getPaymentNotification(
    @Body(new ValidationPipe({ transform: true }))
    body: any,
  ): Promise<any> {
    return await this.appService.getPaymentNotification(body);
  }
}
